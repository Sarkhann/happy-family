import java.util.Arrays;

public class Family {
    private Human father;
    private Human mother;
    private Human[] children;
    private Pet pet;

    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
    }
    public Human getFather() {
        return father;
    }
    public boolean deleteChild (int index){
        if ((this.children == null)){
            return false;
        }
        else {
            int i =0;
            Human [] newChildren = new Human[children.length-1];
            children [index] = null;
            for (int j = 0; j < children.length; j++) {
                if (children[j] !=null){
                    newChildren[i] = children[j];
                    i++;
                }
            }
            this.children = newChildren;
            return true;
        }
    }

    public void addChild(Human child){
        Human [] newChild = Arrays.copyOf(children, children.length+1);
        newChild[children.length] = child;
        this.children = newChild;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
}
