import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private String [][] schedule;
    Family family;
    public Human(){

    }
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }

    public void greetPet() {
        System.out.println("Hello, " + pet.getNickName());
    }

    public void describePet() {
        System.out.println("I have an " + pet.getSpecies()
                + "is " + pet.getAge() + "years old, he is "
                + (pet.getTrickLevel() > 50
                ? "very sly"
                : "almost not sly"));
    }

    public String toString() {
        return "Human{name= " + this.name + ", surname=" + this.surname
                + ", year=" + this.year + ", iq=" + this.iq + ", pet" + " schedule=" + Arrays.toString(schedule)+"}";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String [][]getSchedule() {
        return schedule;
    }

    public void setSchedule(String [][] schedule) {
        this.schedule = schedule;
    }
}
