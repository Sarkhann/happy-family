public class Pet {
    private String species;
    private  String nickName;
    private int age;
    private int trickLevel;
    private String [] habits;

    public Pet(){

    }
    public Pet(String species , String nickName){
        this.species = species;
        this.nickName = nickName;
    }
    public Pet(String species, String nickName, int age, int trickLevel, String [] habits) {
        this.species = species;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits  = habits;
    }

    public void eat(){
        System.out.println("I am eating");
    }
    public void respond(){
        System.out.println("Hello, owner. I am "+ this.nickName +". I miss you!");
    }
    public void foul(){
        System.out.println("I need to cover it up");
    }

    public String toString(){
        return species + "{nickname=" + this.nickName + ", age = "+ this.age
                + ", trickLevel= "+ this.trickLevel + ", habits = " +this.habits;
    }

    public void setSpecies(String species){
        this.species = species;
    }
    public String getSpecies(){
        return species;
    }
    public void setNickName (String nickName){
        this.nickName = nickName;
    }
    public String getNickName(){
        return nickName;
    }

    public void setAge(int age){
        this.age = this.age;
    }
    public int getAge(){
        return age;
    }

    public void setTrickLevel(int trickLevel){
        this.trickLevel = trickLevel;
    }
    public int getTrickLevel(){
        return trickLevel;
    }

    public void setHabits(String [] habits){
        this.habits =habits;
    }
    public String [] getHabits(){
        return habits;
    }

}
