public class Main {
    public static void main(String[] args) {

        Pet pet = new Pet("dog", "Rock", 5, 35, new String[]{"eat","sleep"});
        pet.eat();
        pet.foul();
        pet.respond();
        System.out.println(pet.toString());

        System.out.println("");

        Human human = new Human("Michael", "Karleone", 1977, 90, new String[][]{}, pet);
        human.greetPet();
        human.describePet();
        System.out.println(human.toString());
    }
}
